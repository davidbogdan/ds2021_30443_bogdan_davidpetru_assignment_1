export interface User {
  uid: String;
  name: String;
  birthdate: String;
  address: String;
  username: String;
  password: String;
  deviceUidSet: String[];
}
