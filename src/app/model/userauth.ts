export class UserAuth{
    username?:String;
    password?:String;
    jwt?:String;
    role?:String;
    uid?:String;
}