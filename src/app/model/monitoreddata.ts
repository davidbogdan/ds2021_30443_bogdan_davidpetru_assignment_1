export interface MonitoredData {
    date: String;
    energyConsumption: Number;
    sensorUid:String;
  }