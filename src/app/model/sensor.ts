export interface Sensor {
    uid: String;
    description: String;
    max_value: Number;
    deviceUid: String;
  }