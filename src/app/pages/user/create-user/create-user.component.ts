import { Component, OnInit } from '@angular/core';
import { UsersService} from 'src/app/services/user/users.service'
import { FormGroup } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { User } from 'src/app/model/user';
import {Location} from '@angular/common';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  userForm = new FormGroup({
    name: new FormControl('', Validators.required),
    birthdate: new FormControl('',Validators.required),
    address: new FormControl('', Validators.required),
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  })

  constructor(private userService: UsersService, private location: Location) {
  }

  selectedType?: string;

  onSubmit() {
    console.log(this.userForm.value);
    const name = this.userForm.value.name;
    const birthdate = this.userForm.value.birthdate;
    const address = this.userForm.value.address;
    const username = this.userForm.value.username;
    const password = this.userForm.value.password;
    this.userService.addUser({name, birthdate,address,username,password} as User)
      .subscribe(user => {
        this.location.back();
        //  this.Users.push(User);
      });
  }

  ngOnInit(): void {
  }

}
