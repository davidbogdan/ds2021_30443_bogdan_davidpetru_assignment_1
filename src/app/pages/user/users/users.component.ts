import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/user/users.service';
import { MessageService } from 'src/app/services/message.service';
import { User } from 'src/app/model/user';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  closeResult?: string;
  users: User[] = [];

  constructor(private userService: UsersService, private messageService: MessageService, private modalService: NgbModal) {
  }

  selectedType?: string;

  getUsers(): void {
    this.userService.getUsers().subscribe(users => this.users = users);
  }

  // this.trainingSessions=this.trainingService.getTrainings();
  ngOnInit(): void {
    this.getUsers();

  }

  private initDays(): void {
  }

  delete(user: User): void {
    this.users = this.users.filter(h => h !== user);
    console.log(user.uid);
    this.userService.deleteUser(user.uid).subscribe();
  }

  open(content: any, user: User) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      if (result === 'yes') {
        this.delete(user);
      }
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
