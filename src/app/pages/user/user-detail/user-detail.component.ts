import { Component, OnInit,Input } from '@angular/core';
import { User } from 'src/app/model/user';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import { UsersService } from 'src/app/services/user/users.service';
import { Device } from 'src/app/model/device';
import { DeviceService } from 'src/app/services/device/device.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  @Input() user?: User;
  @Input() deviceSet?:Device[];

  constructor(private userService: UsersService,
              private deviceService: DeviceService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  save(): void {
    if (this.user) {
      this.userService.updateUser(this.user)
        .subscribe(() => this.goBack());
    }

  }

  ngOnInit(): void {
    this.getUser();
    this.getDevices();
  }

  getUser(): void {
    const id = String(this.route.snapshot.paramMap.get('id'));
    this.userService.getUser(id)
      .subscribe(user => this.user = user);
  }

  getDevices():void{
    this.deviceService.getDevices()
      .subscribe(devices => this.deviceSet = devices.filter(d=>d.ownerUid==this.user?.uid));
  }
  goBack(): void {
    this.location.back();
  }

}
