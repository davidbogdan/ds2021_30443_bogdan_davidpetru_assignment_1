import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Sensor } from 'src/app/model/sensor';
import { MessageService } from 'src/app/services/message.service';
import { SensorsService } from 'src/app/services/sensor/sensors.service';

@Component({
  selector: 'app-sensors',
  templateUrl: './sensors.component.html',
  styleUrls: ['./sensors.component.css']
})
export class SensorsComponent implements OnInit {

  closeResult?: string;
  sensors: Sensor[] = [];

  constructor(private sensorService: SensorsService, private messageService: MessageService, private modalService: NgbModal) {
  }

  selectedType?: string;

  getSensors(): void {
    this.sensorService.getSensors().subscribe(sensors => this.sensors = sensors);
  }
  ngOnInit(): void {
    this.getSensors();

  }

  private initDays(): void {
  }

  delete(sensor: Sensor): void {
    this.sensors = this.sensors.filter(h => h !== sensor);
    console.log(sensor.uid);
    this.sensorService.deleteSensor(sensor.uid).subscribe();
  }

  open(content: any, sensor: Sensor) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      if (result === 'yes') {
        this.delete(sensor);
      }
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
