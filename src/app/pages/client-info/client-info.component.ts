import { Component, Input, OnInit } from '@angular/core';
import { Device } from 'src/app/model/device';
import {Location} from '@angular/common';
import { DeviceService } from 'src/app/services/device/device.service';
import { User } from 'src/app/model/user';
import { UsersService } from 'src/app/services/user/users.service';
import { ActivatedRoute } from '@angular/router';
import { Sensor } from 'src/app/model/sensor';
import { ClientInfo } from 'src/app/model/clientinfo';
import { SensorsService } from 'src/app/services/sensor/sensors.service';
@Component({
  selector: 'app-client-info',
  templateUrl: './client-info.component.html',
  styleUrls: ['./client-info.component.css']
})
export class ClientInfoComponent implements OnInit {

  userId?: String;
  deviceSet?:Device[];
  clientInfoSet?:ClientInfo[];

  constructor(private userService: UsersService,
              private sensorService: SensorsService,
              private deviceService: DeviceService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  ngOnInit(): void {
    this.getUser();
    this.getDevices();
    this.setClientInfo();
  }

  getUser(): void {
    this.userId = String(this.route.snapshot.paramMap.get('id'));
  }

  getDevices():void{
    this.deviceService.getDevices()
      .subscribe(devices => this.deviceSet = devices.filter(d=>d.ownerUid==this.userId));
  }
  setClientInfo():void{
    var result: ClientInfo[] = [];
    this.sensorService.getSensors()
      .subscribe(sensors => sensors.forEach(s=>this.createClientInfo(s,result,this.deviceSet)));
    this.clientInfoSet=result;
  }
  createClientInfo(sensor: Sensor, result: ClientInfo[], deviceSet?: Device[]): void{
    if(deviceSet){
      for(var device of deviceSet){
        if(device.sensorUid==sensor.uid){
            result.push({device,sensor} as ClientInfo);
            return;
        }
      }
    }
  }
  goBack(): void {
    this.location.back();
  }

}
