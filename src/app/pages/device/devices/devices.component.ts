import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Device } from 'src/app/model/device';
import { MessageService } from 'src/app/services/message.service';
import { DeviceService } from 'src/app/services/device/device.service';

@Component({
  selector: 'app-device',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.css']
})
export class DeviceComponent implements OnInit {

  closeResult?: string;
  devices: Device[] = [];

  constructor(private deviceService: DeviceService, private messageService: MessageService, private modalService: NgbModal) {
  }

  selectedType?: string;

  getDevices(): void {
    this.deviceService.getDevices().subscribe(devices => this.devices = devices);
  }
  ngOnInit(): void {
    this.getDevices();

  }

  private initDays(): void {
  }

  delete(device: Device): void {
    this.devices = this.devices.filter(h => h !== device);
    console.log(device.uid);
    this.deviceService.deleteDevice(device.uid).subscribe();
  }

  open(content: any, device: Device) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      if (result === 'yes') {
        this.delete(device);
      }
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
