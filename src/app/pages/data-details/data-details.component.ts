import { Component, Input, OnInit } from '@angular/core';
import { Device } from 'src/app/model/device';
import {Location} from '@angular/common';
import { DeviceService } from 'src/app/services/device/device.service';
import { User } from 'src/app/model/user';
import { UsersService } from 'src/app/services/user/users.service';
import { ActivatedRoute } from '@angular/router';
import { Sensor } from 'src/app/model/sensor';
import { ClientInfo } from 'src/app/model/clientinfo';
import { SensorsService } from 'src/app/services/sensor/sensors.service';
import { DataInfo } from 'src/app/model/datainfo';

@Component({
  selector: 'app-data-details',
  templateUrl: './data-details.component.html',
  styleUrls: ['./data-details.component.css']
})
export class DataDetailsComponent implements OnInit {

  userId?: String;
  deviceSet?:Device[];
  dataInfoSet?:DataInfo[];

  constructor(private userService: UsersService,
              private sensorService: SensorsService,
              private deviceService: DeviceService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  ngOnInit(): void {
    this.getUser();
    this.getDevices();
    this.setClientInfo();
  }

  getUser(): void {
    this.userId = String(this.route.snapshot.paramMap.get('id'));
  }

  getDevices():void{
    this.deviceService.getDevices()
      .subscribe(devices => this.deviceSet = devices.filter(d=>d.ownerUid==this.userId));
  }
  setClientInfo():void{
    var result: DataInfo[] = [];
    this.sensorService.getSensors()
      .subscribe(sensors => sensors.forEach(s=>this.createDataInfo(s,result,this.deviceSet)));
    this.dataInfoSet=result;
  }
  createDataInfo(sensor: Sensor, result: DataInfo[], deviceSet?: Device[]): void{
    if(deviceSet){
      for(var device of deviceSet){
        if(device.sensorUid==sensor.uid){
            this.sensorService.getMonitoredData(sensor.uid).subscribe(monitoredDatas=>result.push({device,monitoredDatas} as DataInfo));
            return;
        }
      }
    }
  }
  goBack(): void {
    this.location.back();
  }

}
