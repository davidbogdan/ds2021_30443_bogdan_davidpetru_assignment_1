import { Injectable } from '@angular/core';
import {Device} from 'src/app/model/device';
import {Observable, of} from 'rxjs';
import {MessageService} from 'src/app/services/message.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {environment} from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class DeviceService {
  private devicesUrl = `${environment.apiURL}/devices`;

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`DevicesService: ${message}`);
  }

  constructor(private http: HttpClient, private messageService: MessageService) {
  }

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  getDevices(): Observable<Device[]> {
    return this.http.get<Device[]>(this.devicesUrl)
      .pipe(
        tap(_ => this.log('fetched devices')),
        catchError(this.handleError<Device[]>('getDevices', []))
      )
      ;
  }

  /** GET hero by id. Will 404 if id not found */
  getDevice(id: String): Observable<Device> {
    const url = `${this.devicesUrl}/${id}`;
    return this.http.get<Device>(url).pipe(
      tap(_ => this.log(`fetched device id=${id}`)),
      catchError(this.handleError<Device>(`getDevice id=${id}`))
    );
  }

  /** PUT: update the hero on the server */
  updateDevice(device: Device): Observable<any> {
    const url = `${this.devicesUrl}/${device.uid}`;
    return this.http.put(url, device, this.httpOptions).pipe(
      tap(_ => this.log(`updated device id=${device.uid}`)),
      catchError(this.handleError<any>('updateDevice'))
    );
  }

  /** POST: add a new hero to the server */
  addDevice(device: Device): Observable<Device> {
    return this.http.post<Device>(this.devicesUrl, device, this.httpOptions).pipe(
      tap((newHero: Device) => this.log(`added device w/ id=${newHero.uid}`)),
      catchError(this.handleError<Device>('addDevice'))
    );
  }

  deleteDevice(id: String): Observable<Device> {
    const url = `${this.devicesUrl}/${id}`;

    return this.http.delete<Device>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted device id=${id}`)),
      catchError(this.handleError<Device>('deleteDevice'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
