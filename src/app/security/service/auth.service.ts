import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { UserAuth } from 'src/app/model/userauth';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUserSubject: BehaviorSubject<UserAuth>;
  public currentUser: Observable<UserAuth>;
  private currentUserKey = 'currentUser';

  constructor(
      private http: HttpClient,
      private router: Router
  ) {
      this.currentUserSubject = new BehaviorSubject<UserAuth>(JSON.parse(<string>localStorage.getItem(this.currentUserKey)));
      this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): UserAuth {
      return this.currentUserSubject.value;
  }
  login(email: string, password: string): Observable<UserAuth> {
      return this.http.post<UserAuth>(`${environment.apiURL}/auth`, {
          'username': email,
          'password': password
      })
          .pipe(
              map(user => {
                  localStorage.setItem(this.currentUserKey, JSON.stringify(user));
                  this.currentUserSubject.next(user);
                  this.router.navigate(['/']);
                  return user;
              })
          );
  }

  logout(): void {
      localStorage.removeItem(this.currentUserKey);
      this.router.navigate(['/login']);
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      this.currentUserSubject.next(null);
  }
}