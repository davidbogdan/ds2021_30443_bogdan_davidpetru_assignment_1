import { TestBed } from '@angular/core/testing';

import { RegiterGuard } from './regiter.guard';

describe('RegiterGuard', () => {
  let guard: RegiterGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(RegiterGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
