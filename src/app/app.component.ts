import { Component } from '@angular/core';
import { ActivatedRoute, RouteConfigLoadStart, Router } from '@angular/router';
import { UserAuth } from './model/userauth';
import { AuthService } from './security/service/auth.service';
import { UsersService } from './services/user/users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'energytool';
  currentUser!:UserAuth;
  isAdmin!:boolean;
  loading = false;

  constructor(
    private router : Router,
    private loginService : AuthService,
    private userService: UsersService,
    private route: ActivatedRoute,
  ){
    this.loginService.currentUser.subscribe(user=>{
      this.currentUser=user;
      if(user && user.role){
        if( user.role=='ADMIN'){
          this.isAdmin=true;
        }
        else{
          this.isAdmin=false;
        }
      } 
    });
  }
  logout():void{
    this.loginService.logout();
  }
}
