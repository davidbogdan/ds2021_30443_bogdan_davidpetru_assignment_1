package ro.bogdan.energytool.device.dto;

import lombok.*;
import ro.bogdan.energytool.sensor.model.Sensor;
import ro.bogdan.energytool.user.model.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class DeviceDto {
    private String uid;
    private String description;
    private String location;
    private Integer maxConsumption;
    private Integer averageConsumption;
    private String ownerUid;
    private String sensorUid;
}
