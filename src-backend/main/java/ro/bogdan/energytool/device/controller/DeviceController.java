package ro.bogdan.energytool.device.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.bogdan.energytool.device.dto.DeviceDto;
import ro.bogdan.energytool.device.service.DeviceService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/devices")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DeviceController {
    private final DeviceService deviceService;

    @GetMapping("")
    public ResponseEntity<List<DeviceDto>> findAll(){
        return ResponseEntity.ok(deviceService.findAll());
    }
    @PostMapping("")
    public ResponseEntity<DeviceDto> create(@RequestBody DeviceDto deviceDto){
        return new ResponseEntity<>(deviceService.save(deviceDto), HttpStatus.CREATED);
    }
    @GetMapping("/{deviceId}")
    public ResponseEntity<DeviceDto> findByUid(@PathVariable String deviceId){
        return new ResponseEntity<>(deviceService.findByUid(deviceId),HttpStatus.OK);
    }
    @PutMapping("/{deviceId}")
    public ResponseEntity<DeviceDto> update(@RequestBody DeviceDto deviceDto, @PathVariable String deviceId){
        return new ResponseEntity<>(deviceService.update(deviceDto,deviceId),HttpStatus.OK);
    }
    @DeleteMapping("/{deviceId}")
    public ResponseEntity<?> delete(@PathVariable String deviceId){
        deviceService.delete(deviceId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
