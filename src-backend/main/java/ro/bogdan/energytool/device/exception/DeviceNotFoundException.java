package ro.bogdan.energytool.device.exception;

public class DeviceNotFoundException extends RuntimeException{
    public DeviceNotFoundException(String uid){
        super("Could not find device with following uid: "+uid);
    }
}
