package ro.bogdan.energytool.device.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ro.bogdan.energytool.device.exception.DeviceNotFoundException;
import ro.bogdan.energytool.sensor.exception.SensorNotFoundException;

@ControllerAdvice
public class DeviceExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {DeviceNotFoundException.class})
    public ResponseEntity<Object> handleDeviceNotFoundException(DeviceNotFoundException ex){
        return new ResponseEntity<>(ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }
}
