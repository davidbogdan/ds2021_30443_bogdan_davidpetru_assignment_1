package ro.bogdan.energytool.device.model;

import lombok.*;
import ro.bogdan.energytool.sensor.model.Sensor;
import ro.bogdan.energytool.user.model.User;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String uid;
    private String description;
    private String location;
    private Integer maxConsumption;
    private Integer averageConsumption;
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User owner;

    @OneToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "sensor_id", referencedColumnName = "id")
    private Sensor sensor;
}
