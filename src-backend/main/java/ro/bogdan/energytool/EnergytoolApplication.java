package ro.bogdan.energytool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnergytoolApplication {

    public static void main(String[] args) {
        SpringApplication.run(EnergytoolApplication.class, args);
    }

}
