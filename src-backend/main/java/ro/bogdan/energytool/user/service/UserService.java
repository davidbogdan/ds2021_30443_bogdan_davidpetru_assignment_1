package ro.bogdan.energytool.user.service;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ro.bogdan.energytool.device.exception.DeviceNotFoundException;
import ro.bogdan.energytool.device.model.Device;
import ro.bogdan.energytool.device.repository.DeviceRepo;
import ro.bogdan.energytool.user.dto.UserDto;
import ro.bogdan.energytool.user.exception.UserNotFoundException;
import ro.bogdan.energytool.user.exception.UsernameAlreadyExist;
import ro.bogdan.energytool.user.model.Role;
import ro.bogdan.energytool.user.model.User;
import ro.bogdan.energytool.user.repository.UserRepo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserService {
    private final UserRepo userRepo;
    private final DeviceRepo deviceRepo;
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;
    public UserDto save(UserDto userDto){
        checkIfUsernameIsAvailable(userDto.getUsername());
        userDto.setUid(UUID.randomUUID().toString());
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        User user = fromDtoToEntity(userDto);
        user.setRole(Role.USER);
        return fromEntityToDto(userRepo.save(user));
    }

    public List<UserDto> findAll(){
        return userRepo.findAll().stream().filter(u->u.getRole()==Role.USER).map(this::fromEntityToDto).collect(Collectors.toList());
    }

    public UserDto findByUid(String uid){
        return fromEntityToDto(userRepo.findByUid(uid).orElseThrow(()->new UserNotFoundException(uid)));
    }

    public UserDto update(UserDto userDto, String uid){
        if(userDto.getUsername()!=null)
            checkIfUsernameIsAvailable(userDto.getUsername());
        User oldUser = userRepo.findByUid(uid).orElseThrow(()->new UserNotFoundException(uid));
        return fromEntityToDto(userRepo.save(updateUserFromDto(oldUser,userDto)));
    }

    public void delete(String uid){
        User user = userRepo.findByUid(uid).orElseThrow(()->new UserNotFoundException(uid));
        userRepo.delete(user);
    }

    private User updateUserFromDto(User user, UserDto userDto){
        if(userDto.getDeviceUidSet()!=null){
            Set<Device> deviceSet = new HashSet<>();
            userDto.getDeviceUidSet().forEach(uid->deviceSet.add(deviceRepo.findByUid(uid).orElseThrow(()-> new DeviceNotFoundException(uid))));
            user.setDeviceSet(deviceSet);
        }
        if(userDto.getAddress()!=null){
            user.setAddress(userDto.getAddress());
        }
        if(userDto.getBirthdate()!=null){
            user.setBirthdate(userDto.getBirthdate());
        }
        if(userDto.getName()!=null){
            user.setName(userDto.getName());
        }
        return user;
    }
    private User fromDtoToEntity(UserDto userDto){
        User user  = modelMapper.map(userDto,User.class);
        Set<Device> deviceSet = new HashSet<>();
        if(userDto.getDeviceUidSet()!=null)
            userDto.getDeviceUidSet().forEach(uid->deviceSet.add(deviceRepo.findByUid(uid).orElseThrow(()-> new DeviceNotFoundException(uid))));
        user.setDeviceSet(deviceSet);
        return user;
    }

    private UserDto fromEntityToDto(User user){
        UserDto userDto = modelMapper.map(user,UserDto.class);
        Set<String> deviceUidSet = new HashSet<>();
        if(user.getDeviceSet()!=null)
            user.getDeviceSet().forEach(d->deviceUidSet.add(d.getUid()));
        userDto.setDeviceUidSet(deviceUidSet);
        return userDto;
    }
    private void checkIfUsernameIsAvailable(String username){
        if(userRepo.findByUsername(username).isPresent())
            throw new UsernameAlreadyExist(username);
    }
}
