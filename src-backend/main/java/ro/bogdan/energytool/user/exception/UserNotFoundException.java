package ro.bogdan.energytool.user.exception;

public class UserNotFoundException extends RuntimeException{
    public UserNotFoundException(String uid){
        super("Could not find user with following uid: "+uid);
    }
}
