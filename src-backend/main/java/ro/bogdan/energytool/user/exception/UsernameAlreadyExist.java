package ro.bogdan.energytool.user.exception;

public class UsernameAlreadyExist extends RuntimeException{
    public UsernameAlreadyExist(String username){
        super("A user with this username: "+username + " already exists");
    }
}
