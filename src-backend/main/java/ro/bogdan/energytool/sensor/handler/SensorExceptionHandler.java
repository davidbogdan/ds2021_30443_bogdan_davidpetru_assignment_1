package ro.bogdan.energytool.sensor.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ro.bogdan.energytool.sensor.exception.SensorNotFoundException;
import ro.bogdan.energytool.user.exception.UserNotFoundException;

@ControllerAdvice
public class SensorExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {SensorNotFoundException.class})
    public ResponseEntity<Object> handleSensorNotFoundException(SensorNotFoundException ex){
        return new ResponseEntity<>(ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }
}
