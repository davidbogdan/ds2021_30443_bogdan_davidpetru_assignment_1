package ro.bogdan.energytool.sensor.service;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.bogdan.energytool.device.exception.DeviceNotFoundException;
import ro.bogdan.energytool.device.model.Device;
import ro.bogdan.energytool.device.repository.DeviceRepo;
import ro.bogdan.energytool.sensor.dto.MonitoredDataDto;
import ro.bogdan.energytool.sensor.dto.SensorDto;
import ro.bogdan.energytool.sensor.model.MonitoredData;
import ro.bogdan.energytool.sensor.model.Sensor;
import ro.bogdan.energytool.sensor.exception.SensorNotFoundException;
import ro.bogdan.energytool.sensor.repository.MonitoredDataRepo;
import ro.bogdan.energytool.sensor.repository.SensorRepo;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SensorService {
    private final DeviceRepo deviceRepo;
    private final SensorRepo sensorRepo;
    private final ModelMapper modelMapper;
    private final MonitoredDataRepo monitoredDataRepo;

    public SensorDto save(SensorDto sensorDto){
        sensorDto.setUid(UUID.randomUUID().toString());
        Sensor sensor = sensorRepo.save(fromDtoToEntity(sensorDto));
        //Sensor sensor = fromDtoToEntity(sensorDto);
        //sensor.setDevice(null);
        return fromEntityToDto(sensorRepo.save(sensor));//fromEntityToDto(updateParentDevice(sensor,sensorDto));
    }

    public List<SensorDto> findAll(){
        return sensorRepo.findAll().stream().map(this::fromEntityToDto).collect(Collectors.toList());
    }

    public SensorDto findByUid(String uid){
        return fromEntityToDto(sensorRepo.findByUid(uid).orElseThrow(()->new SensorNotFoundException(uid)));
    }

    public SensorDto update(SensorDto sensorDto, String uid){
        Sensor oldsensor = sensorRepo.findByUid(uid).orElseThrow(()->new SensorNotFoundException(uid));
        return fromEntityToDto(sensorRepo.save(updateUserFromDto(oldsensor,sensorDto)));
    }

    public void delete(String uid){
        Sensor sensor = sensorRepo.findByUid(uid).orElseThrow(()->new SensorNotFoundException(uid));
        if(sensor.getDevice()!=null){
            Device device = sensor.getDevice();
            deviceRepo.delete(device);
        }

        sensorRepo.delete(sensor);
    }

    public Set<MonitoredDataDto> getAllMonitoredData(String uid){
        Sensor sensor = sensorRepo.findByUid(uid).orElseThrow(()->new SensorNotFoundException(uid));
        return sensor.getMonitoredDataSet().stream().map(this::fromEntityToDto).collect(Collectors.toSet());
    }
    public MonitoredDataDto saveMonitoredData(String uid, MonitoredDataDto monitoredDataDto){
        Sensor sensor = sensorRepo.findByUid(uid).orElseThrow(()->new SensorNotFoundException(uid));
        monitoredDataDto.setSensorUid(uid);
        MonitoredData monitoredData = monitoredDataRepo.save(fromDtoToEntity(monitoredDataDto));
        sensor.addData(monitoredData);
        sensorRepo.save(sensor);
        updateDeviceConsumption(sensor);
        return fromEntityToDto(monitoredData);
    }

    private void updateDeviceConsumption(Sensor sensor){
        Device device = sensor.getDevice();
        Integer sum = sensor.getMonitoredDataSet().stream().map(MonitoredData::getEnergyConsumption).reduce(0, Integer::sum);
        device.setAverageConsumption(sum/sensor.getMonitoredDataSet().size());
        device.setMaxConsumption(sensor.getMonitoredDataSet().stream().mapToInt(MonitoredData::getEnergyConsumption).max().orElseThrow(NoSuchElementException::new));
        deviceRepo.save(device);
    }
    private Sensor updateParentDevice(Sensor sensor, SensorDto sensorDto){
        if(sensorDto.getDeviceUid()!=null){
            Device device = deviceRepo.findByUid(sensorDto.getDeviceUid()).orElseThrow(()-> new DeviceNotFoundException(sensorDto.getDeviceUid()));
            device.setSensor(sensor);
            deviceRepo.save(device);
            sensor.setDevice(device);
            return sensorRepo.save(sensor);
        }
        return sensor;
    }
    private Sensor updateUserFromDto(Sensor sensor, SensorDto sensorDto){
        if(sensorDto.getDescription()!=null){
            sensor.setDescription(sensorDto.getDescription());
        }
        if(sensorDto.getMax_value()!=null){
            sensor.setMax_value(sensorDto.getMax_value());
        }
        if(sensorDto.getDeviceUid()!=null){
            sensor.setDevice(deviceRepo.findByUid(sensorDto.getDeviceUid()).orElseThrow(()-> new DeviceNotFoundException(sensorDto.getDeviceUid())));

        }
        return sensor;
    }
    private Sensor fromDtoToEntity(SensorDto sensorDto){
        Sensor sensor  = modelMapper.map(sensorDto,Sensor.class);
        if(sensorDto.getMonitoredDataSet()!=null)
            sensor.setMonitoredDataSet(sensorDto.getMonitoredDataSet().stream().map(this::fromDtoToEntity).collect(Collectors.toSet()));
        if(sensorDto.getDeviceUid()!=null)
            sensor.setDevice(deviceRepo.findByUid(sensorDto.getDeviceUid()).orElseThrow(()-> new DeviceNotFoundException(sensorDto.getDeviceUid())));
        return sensor;
    }

    private SensorDto fromEntityToDto(Sensor sensor){
        SensorDto sensorDto = modelMapper.map(sensor,SensorDto.class);
        Device device = sensor.getDevice();
        if(device!=null)
            sensorDto.setDeviceUid(device.getUid());
        if(sensor.getMonitoredDataSet()!=null)
            sensorDto.setMonitoredDataSet(sensor.getMonitoredDataSet().stream().map(this::fromEntityToDto).collect(Collectors.toSet()));
        return sensorDto;
    }
    private MonitoredData fromDtoToEntity(MonitoredDataDto monitoredDataDto){
        MonitoredData monitoredData = modelMapper.map(monitoredDataDto,MonitoredData.class);
        if(monitoredDataDto.getSensorUid()!=null)
            monitoredData.setSensor(sensorRepo.findByUid(monitoredDataDto.getSensorUid()).orElseThrow(()->new SensorNotFoundException(monitoredDataDto.getSensorUid())));
        return monitoredData;
    }
    private MonitoredDataDto fromEntityToDto(MonitoredData monitoredData){
        MonitoredDataDto monitoredDataDto = modelMapper.map(monitoredData,MonitoredDataDto.class);
        if(monitoredData.getSensor()!=null)
            monitoredDataDto.setSensorUid(monitoredData.getSensor().getUid());
        return monitoredDataDto;
    }
}
