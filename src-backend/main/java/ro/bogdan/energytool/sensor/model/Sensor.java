package ro.bogdan.energytool.sensor.model;

import lombok.*;
import ro.bogdan.energytool.device.model.Device;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
public class Sensor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String uid;
    private String description;
    private Integer max_value;
    @OneToOne(mappedBy = "sensor")
    private Device device;
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "sensor",orphanRemoval = true)
    private Set<MonitoredData> monitoredDataSet;


    public void addData(MonitoredData monitoredData){
        monitoredDataSet.add(monitoredData);
    }
}
