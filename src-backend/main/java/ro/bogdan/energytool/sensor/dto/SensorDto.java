package ro.bogdan.energytool.sensor.dto;

import lombok.*;
import ro.bogdan.energytool.device.model.Device;
import ro.bogdan.energytool.sensor.model.MonitoredData;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class SensorDto {
    private String uid;
    private String description;
    private Integer max_value;
    private String deviceUid;
    private Set<MonitoredDataDto> monitoredDataSet;
}
