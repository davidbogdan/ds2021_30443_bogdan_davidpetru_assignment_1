package ro.bogdan.energytool.security.service;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ro.bogdan.energytool.security.exception.NoAuthorityException;
import ro.bogdan.energytool.security.model.AuthRequest;
import ro.bogdan.energytool.security.model.AuthResponse;
import ro.bogdan.energytool.security.util.JwtUtils;
import ro.bogdan.energytool.security.util.UserDetailsImpl;
import ro.bogdan.energytool.user.repository.UserRepo;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Service
@Setter
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuthService {
    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;
    private final UserDetailsService userDetailsService;

    public AuthResponse authenticateUser(AuthRequest authRequest) {
        System.out.println("Auth0");
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                authRequest.getUsername(), authRequest.getPassword()
        ));
        System.out.println("Auth1");
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = jwtUtils.generateJwtToken(authentication);
        System.out.println("Auth2");
        UserDetailsImpl toBeLoggedInUser = (UserDetailsImpl) authentication.getPrincipal();
        System.out.println("Auth3");
        String role = toBeLoggedInUser.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .findFirst()
                .orElseThrow(() -> new NoAuthorityException(toBeLoggedInUser.getUsername()));
        System.out.println("Auth4");
        return AuthResponse.builder()
                .jwt(token)
                .role(role)
                .username(toBeLoggedInUser.getUsername())
                .uid(toBeLoggedInUser.getUid())
                .build();
    }

    private String parseJwt(HttpServletRequest request) {
        String headerAuth = request.getHeader("Authorization");

        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(7);
        }
        return null;
    }
}