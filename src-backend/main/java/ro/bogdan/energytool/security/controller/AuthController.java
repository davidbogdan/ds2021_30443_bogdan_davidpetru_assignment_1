package ro.bogdan.energytool.security.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ro.bogdan.energytool.security.model.AuthRequest;
import ro.bogdan.energytool.security.model.AuthResponse;
import ro.bogdan.energytool.security.service.AuthService;
import ro.bogdan.energytool.user.dto.UserDto;
import ro.bogdan.energytool.user.service.UserService;

@RestController
@CrossOrigin
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuthController {
    private final AuthService authService;
    private final UserService userService;


    @PostMapping("/auth")
    public ResponseEntity<AuthResponse> signIn(@RequestBody AuthRequest authRequest) {
      //  System.out.println("SIGNIN IN PULA");
        return ResponseEntity.ok(authService.authenticateUser(authRequest));
    }
    @PostMapping("/signup")
    public ResponseEntity<UserDto> signUp(@RequestBody UserDto userDto){
        return new ResponseEntity<>(userService.save(userDto), HttpStatus.CREATED);
    }
}
