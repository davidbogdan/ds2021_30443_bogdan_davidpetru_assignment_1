package ro.bogdan.energytool.security.util;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ro.bogdan.energytool.user.exception.UserNotFoundException;
import ro.bogdan.energytool.user.model.User;
import ro.bogdan.energytool.user.repository.UserRepo;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepo userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        System.out.println("FindByUsername");
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
//        System.out.println(user.getUsername());
//        System.out.println(user.getPassword());
//        System.out.println(user.getRole().toString());
        return UserDetailsImpl.build(user);
    }
}
