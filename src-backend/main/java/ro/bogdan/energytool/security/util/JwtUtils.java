package ro.bogdan.energytool.security.util;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.security.core.Authentication;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

@Component
@Setter
@Getter
@ConfigurationProperties(prefix = "app")
public class JwtUtils {
    private String jwtSecret;

    private String jwtExpirationMs;

    public String generateJwtToken(Authentication authentication) {
        UserDetailsImpl user = (UserDetailsImpl) authentication.getPrincipal();
        return createToken(user.getUsername(), jwtExpirationMs);
    }

    public String getUsernameFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }

    public boolean isValidJwtToken(String token) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private String createToken(String username, String jwtExpirationsTime) {
        Date now = new Date(System.currentTimeMillis());
        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(now)
                .setExpiration(new Date(now.getTime() + Long.parseLong(jwtExpirationsTime)))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }
}
