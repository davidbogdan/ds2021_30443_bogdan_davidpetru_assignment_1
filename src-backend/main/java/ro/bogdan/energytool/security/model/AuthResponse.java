package ro.bogdan.energytool.security.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class AuthResponse {
    private String jwt;
    private String username;
    private String role;
    private String uid;
}
