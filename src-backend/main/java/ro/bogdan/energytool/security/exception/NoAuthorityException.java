package ro.bogdan.energytool.security.exception;

public class NoAuthorityException extends RuntimeException {
    public NoAuthorityException(String username) {
        super("Could not find any authority for user with username: " + username);
    }
}
