package ro.bogdan.energytool.security;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ro.bogdan.energytool.user.model.Role;
import ro.bogdan.energytool.user.model.User;
import ro.bogdan.energytool.user.repository.UserRepo;

import java.util.Date;
import java.util.HashSet;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class Bootstrapper implements ApplicationListener<ApplicationReadyEvent> {
    private final UserRepo userRepo;
    private final PasswordEncoder passwordEncoder;
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        if(userRepo.findByRole(Role.ADMIN).isEmpty()){
            User bootstrappedAdmin = User.builder()
                                        .id(9999L)
                                        .username("admin")
                                        .name("admin")
                                        .uid("admin")
                                        .password(passwordEncoder.encode("admin"))
                                        .address("adminAddress")
                                        .birthdate(new Date())
                                        .deviceSet(new HashSet<>())
                                        .role(Role.ADMIN)
                                        .build();
            userRepo.save(bootstrappedAdmin);
        }
    }
}
